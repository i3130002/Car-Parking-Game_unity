﻿using UnityEngine;
using System.Collections;
 
public class move : MonoBehaviour
{

		// Use this for initialization
		void Start ()
		{
	
		}

		public int times = 1;
		public int  rotate = 1;
		public int max_rotate = 45;
		public float degree_weel = globals.degree_wheel;
		// Update is called once per frame
		void Update ()
		{

            if (globals.game_is_on == false)
                return;
				if (Input.GetKey (globals.turn_left)) {

						if (degree_weel < max_rotate - rotate * Time.deltaTime) {
								degree_weel += rotate * Time.deltaTime;
								gameObject.BroadcastMessage ("degree_weel", degree_weel + transform.eulerAngles.z);
             
								GameObject.Find ("/wheel zone/wheel").SendMessage ("degree_weel", degree_weel);
			
						}
						//transform.Rotate(0,0,rotate	*Time.deltaTime);
				} else if (Input.GetKey (globals.turn_right)) {
						//transform.Translate (new Vector3 (-1*times * Time.deltaTime, 0, 0));
						if (degree_weel > -1 * max_rotate + rotate * Time.deltaTime) {

								degree_weel -= rotate * Time.deltaTime;
								gameObject.BroadcastMessage ("degree_weel", degree_weel + transform.eulerAngles.z);
								GameObject.Find ("/wheel zone/wheel").SendMessage ("degree_weel", degree_weel);
			
						}			
						//transform.Rotate(0,0,-1*rotate	*Time.deltaTime);
				} 
				float rot = degree_weel;

				if (globals.speed!=0) {
						if (!Input.GetKey (globals.turn_right) && !Input.GetKey (globals.turn_left)) {//######auto rotate to 0
								if (degree_weel < 2 && degree_weel > -2)
										degree_weel = 0;
								else if (degree_weel > 0)
										degree_weel -= rotate * Time.deltaTime * Mathf.Abs( globals.speed) * globals.wheel_reset_speed;
								else if (degree_weel < 0)
										degree_weel += rotate * Time.deltaTime * Mathf.Abs( globals.speed) * globals.wheel_reset_speed;
				
								//						gameObject.BroadcastMessage ("degree_weel", degree_weel + transform.eulerAngles.z);
								//						GameObject.Find ("/wheel zone/wheel").SendMessage ("degree_weel", degree_weel);
				
						}
			
				}
		if (Input.GetKey (globals.break_)) {
			if (0 <= globals.speed + globals.break_speed * Time.deltaTime)
				globals.speed += globals.break_speed * Time.deltaTime;
			else globals.speed=0;
			
		}
			 else if (Input.GetKey (globals.gass_reverse)) {
						if (globals.inverse_max_speed <= globals.speed - globals.decreasing_acceleration * Time.deltaTime)
								globals.speed -= globals.decreasing_acceleration * Time.deltaTime;
			  
		} else 	if (Input.GetKey (globals.gass)) {
			
			if (globals.max_speed >= globals.acceleration * Time.deltaTime + globals.speed)
				globals.speed += globals.acceleration * Time.deltaTime;
			
			
		}

				//####### move with speed of yours
				{
						if (!Input.GetKey (globals.gass) && !Input.GetKey (globals.gass_reverse)) {
								if (globals.speed > 0){
								if (globals.speed - globals.decreasing_acceleration * Time.deltaTime >= 0)
										globals.speed -= globals.decreasing_acceleration * Time.deltaTime;
								else
										globals.speed = 0;
								}else if (globals.speed < 0)
								if (globals.speed + globals.decreasing_acceleration * Time.deltaTime <= 0)
										globals.speed += globals.decreasing_acceleration * Time.deltaTime;
								else
										globals.speed = 0;

						}
						gameObject.SendMessageUpwards ("translate_car_parent", globals.speed * times * Time.deltaTime);
						gameObject.SendMessageUpwards ("rotate_car_parent", (globals.speed) * rot * Time.deltaTime);
					
						gameObject.BroadcastMessage ("degree_weel", degree_weel + transform.eulerAngles.z);
						GameObject.Find ("/wheel zone/wheel").SendMessage ("degree_weel", degree_weel);
				}
			
	
            
		}

		void OnCollisionEnter2D (Collision2D coll)
		{
				Debug.Log ("28");

		}

}
