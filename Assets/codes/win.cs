﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class win : MonoBehaviour {
	public  bool  in_win_zone=false;
	public int in_around_win_zone=0;
	protected    int current_level;
    InputField i;
	void 	OnTriggerEnter2D(Collider2D other) {
	//	Destroy(other.gameObject);
		//Debug.Log ("colide en:"+other.transform.name);
		if (other.transform.name == "parking_zone")
						in_win_zone = true;
		else if(other.transform.name  == "around_parking_zone")
						in_around_win_zone ++;
			
	}
    
	
	void OnTriggerExit2D(Collider2D other) {
		//Debug.Log (other.transform.name);
		
		//Destroy(other.gameObject);
		if (other.transform.name == "parking_zone")
						in_win_zone = false;
				else if (other.transform.name == "around_parking_zone") {
						in_around_win_zone --;
						
				}
		
	}


    // Use this for initialization
    void Start()
    {

        Text t = GameObject.Find("username").GetComponent<Text>();
        t.text = " sss";      
      submit_score=  GameObject.Find("submit_score");
      submit_score.SetActive(false);


      game_win = false;
      
    }
    GameObject submit_score;
    bool game_win;
    // Update is called once per frame

    void Update()
    {

        if (in_win_zone == true && in_around_win_zone == 0 && globals.speed == 0 && !game_win)
        {
      game_win = true;
      globals.game_is_on = false;		
						GameObject.Find("notification").transform.position=new Vector3(GameObject.Find("notification").transform.position.x,GameObject.Find("notification").transform.position.y,globals.notification_location.z);
						end_of_level=true;
                        submit_score.SetActive(true);
                        InputField i = GameObject.Find("InputField").GetComponent<InputField>();
                        GameObject.Find("SText").GetComponent<Text>().text = "Your score is:" + globals.time_spent+" second";
                        if (globals.player_username.Length >0)
                        {
                            i.text = globals.player_username;

                        }
							}
         
    }

    public bool end_of_level = false;
	
//	void OnGUI() {
//		
//		if(end_level_1_value){
//			GUIStyle style=new GUIStyle();
//			style.normal.textColor = Color.black;
//			style.fontSize=40;
//			style.fontStyle=FontStyle.Normal;
//
//
//			Vector3 point;
//			point=Camera.main.WorldToScreenPoint( GameObject.Find("notification").transform.position);
//
//			GUI.Label (new Rect (point.x-100,point.y-60, 100, 100), "you done level one congrate",style);
//			if(GUI.Button(new Rect(20,40,80,20), "Go to level 2")) {
//				Debug.Log("end of level 1");
//			}
//		}
//	}


    public void save_score()
    {
        
        System.IO.StreamWriter sw = new System.IO.StreamWriter("scores\\score_level" + globals.current_level,true,System.Text.Encoding.UTF8);
    
        Text t = GameObject.Find("username").GetComponent<Text>();
        if (t.text.Length > 0)
        {
            globals.player_username = t.text;
        sw.WriteLine(t.text + ";;;;;;;;;;" + globals.time_spent);
        }
           globals.time_spent=0;
        sw.Close();

        globals.level_restarted = false;

        if (globals.current_level < globals.max_levels)
            SceneManager.LoadScene("level" + ++globals.current_level);
        else
            SceneManager.LoadScene("level_end");
    }
	 
}
