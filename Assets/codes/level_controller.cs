﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class level_controller : MonoBehaviour
{

	// Use this for initialization
	void Start () {
        
        Debug.Log(globals.current_level.ToString());
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKey(KeyCode.Z))
        {
            //Application.LoadLevel();
            Debug.Log(globals.time_spent+"");
        }

        if (Input.GetKey(globals.reload_level) && globals.game_is_on)
        {
            //Application.LoadLevel();
            globals.level_restarted = true ;

            SceneManager.LoadScene("level" + globals.current_level);
        }


        if (Input.GetKey(globals.next_level) && globals.game_is_on)
        {
            if (globals.max_levels > globals.current_level)
            {
                globals.current_level++;
                 
                SceneManager.LoadScene("level" + globals.current_level);

            }
        }
        if (Input.GetKey(globals.previus_level) && globals.game_is_on)
        {
            if (globals.min_levels < globals.current_level)
            {
                globals.current_level--;

                SceneManager.LoadScene("level" + globals.current_level);

            }
        }
        if (Input.GetKeyDown(globals.log)   && globals.game_is_on)
        {
            Debug.Log(globals.player_username);
            
        }

        if (Input.GetKeyDown(globals.god_mode_key) && globals.game_is_on)
        {
            globals.god_mode = !globals.god_mode;
        }
	}
}
