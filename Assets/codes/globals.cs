﻿using UnityEngine;
using System.Collections;

public static class globals  {
    public static float time_spent = 0;


	public static  int current_level=0 ;
	public static int max_levels =4;
    public static int min_levels =1;

    public static Vector3 notification_location;

    public static bool game_is_on = false;

	public static float speed=0;
    public static float degree_wheel = 0;
	public static float max_speed=4f;
	public static float inverse_max_speed=-2f;
	public static float break_speed=-2f;

	public static int acceleration=1;
	public static int decreasing_acceleration=1;
	 

	public static int wheel_reset_speed=1;

	public static bool god_mode=false;



    //keys
    public static KeyCode confirm = KeyCode.Space;
    public static KeyCode break_ = KeyCode.B;
    public static KeyCode reload_level = KeyCode.R;
    public static KeyCode next_level=KeyCode.T;
    public static KeyCode previus_level = KeyCode.E;
    public static KeyCode gass = KeyCode.UpArrow;
    public static KeyCode gass_reverse = KeyCode.DownArrow;
    public static KeyCode turn_right= KeyCode.RightArrow;
    public static KeyCode turn_left = KeyCode.LeftArrow;
    public static KeyCode log = KeyCode.L;
    public static KeyCode end_level= KeyCode.F3;

    //player name
    public static string player_username="";

    //reset _game
    public static bool level_restarted = false;

    //god mode
    public static KeyCode god_mode_key= KeyCode.G;
     
}
