﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class lose : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (user_loses && Input.GetKey(globals.confirm))
        {
            globals.level_restarted = true;
            SceneManager.LoadScene("level" + globals.current_level);

        }
    }
	bool user_loses=false;
	void user_lose(){
		this.transform.position = new Vector3 (this.transform.position.x, this.transform.position.y, -9);
		user_loses = true;
        globals.game_is_on = false;
	}
}